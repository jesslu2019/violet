import { Injectable } from "@angular/core";

/**
 * Angular service that serves as a global registry for modal components.
 */
@Injectable({ providedIn: "root" })
export class ModalService {
  modals: Array<any> = [];

  /**
   * Adds the given modal the registry.
   */
  add(modal: any) {
    this.modals.push(modal);
  }

  /**
   * Removes the modal with the given ID from the registry.
   */
  remove(id: string) {
    this.modals = this.modals.filter(x => x.id !== id);
  }

  /**
   * Finds the modal with the given ID, opens it, and returns the
   * modal component.
   */
  open(id: string): any {
    const modal = this.modals.filter(x => x.id === id)[0];
    modal.open();
    return modal;

  }

  /**
   * Finds the modal with the given ID and closes it.
   */
  close(id: string) {
    this.modals.filter(x => x.id === id)[0].close();
  }
}
