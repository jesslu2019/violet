import { Trade } from './trade';

/**
 * Deserializable encapsulation of a trader's position.
 *
 * @author Will Provost
 */
export class Position {
  openingTrade: Trade;
  closingTrade: Trade;
  profitOrLoss: number;
  ROI: number;

  constructor(openingTrade: Trade, closingTrade: Trade,
      profitOrLoss: number, ROI: number) {
    this.openingTrade = openingTrade;
    this.closingTrade = closingTrade;
    this.profitOrLoss = profitOrLoss;
    this.ROI = ROI;
  }
}
