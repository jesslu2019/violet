import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { TradersComponent } from './traders/traders.component';

const routes: Routes = [
  
  { path: '',
  component: OverviewComponent
},
{ path: 'overview',
  component: OverviewComponent
},
{
  path: 'traders',
  component: TradersComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
