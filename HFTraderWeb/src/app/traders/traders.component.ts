import { Component, OnInit } from '@angular/core';
import { TraderTable } from "../../view/trader-table";
import { HistoryComponent } from "../history/history.component"

@Component({
  selector: 'app-traders',
  templateUrl: './traders.component.html',
  styleUrls: ['./traders.component.css']
})
export class TradersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
