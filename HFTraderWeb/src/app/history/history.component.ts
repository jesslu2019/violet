import { Component, OnInit, Input } from '@angular/core';
import { Trader } from "../../model/trader";
import { TraderService } from "../../model/trader-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  @Input() trader: Trader;

  positions;
  service: TraderService;

  constructor(public activeModal: NgbActiveModal) {
    
   }

  ngOnInit() {
  //  this.displayHistory(2);
  }

  // async displayHistory(trader: Trader){
  //   // this.trader = await this.service.getTrader(ID);
  //   // this.positions = this.trader.positions;
  //   // trader.history();
  //   // console.log(this.positions[ID]);
  // }

}
