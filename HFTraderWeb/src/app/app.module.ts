import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderTable } from '../view/trader-table';
import { TraderToolbar } from '../view/trader-toolbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OverviewComponent } from './overview/overview.component';
import { TradersComponent } from './traders/traders.component';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { HistoryComponent } from './history/history.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    TraderTable,
    TraderToolbar,
    OverviewComponent,
    TradersComponent,
    HistoryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    RouterModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [HistoryComponent]
})
export class AppModule { }
