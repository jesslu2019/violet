
When a trade is confirmed, by weay of a reply message from the order broker, what is the path through the application code -- what methods of what classes -- that results in (a) the trade being recorded in the application's database, and (b) the strategy that requested the trade being updated (either with a new open position or with a closing trade for its most recent position?

What is the path through the application's code that lets the user stop a running trader -- from UI through to database?

Where is the database URL found in the application's code base?

What Angular component is responsible for hitting the backing web service and triggering updates in the UI every 15 seconds?

