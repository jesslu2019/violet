package com.citi.trading.order;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.citi.trading.Trade;
import com.citi.trading.jms.PointToPointClient;
import com.citi.trading.mq.TradeFactory;
import com.citi.trading.mq.TradeMessenger;

/**
Unit tests for the {@link OrderBroker} that relies on actual messaging
over ActiveMQ.

@author Will Provost
*/
public class OrderBrokerTest_Reliable
{
    private static TradeMessenger service;
    
    /**
    Start the order broker as a receiver on an ActiveMQ queue.
    */
    @BeforeClass
    public static void setUpClass ()
        throws Exception
    {
        service = new TradeMessenger ();
        QueueReceiver receiver = service.openToReceive ();
        receiver.setMessageListener (new OrderBroker ());
    }
    
    /**
    Close the broker and underlying connections.
    */
    @AfterClass
    public static void tearDownClass ()
        throws Exception
    {
        service.close ();
    }
    
    public static void sendTradeRequest(String correlationID, boolean setReplyTo,
            boolean expectResponse, int sharesExpected, Trade.Result resultExpected)
        throws JMSException
    {
        Trade trade = TradeFactory.createTestPurchase ();
        String XML = TradeMessenger.tradeToXML (trade);
        
        try ( PointToPointClient client = 
            new PointToPointClient (TradeMessenger.REQUEST_QUEUE); )
        {
            try ( PointToPointClient replyClient = 
                new PointToPointClient (TradeMessenger.RESPONSE_QUEUE); )
            {
                QueueReceiver receiver = replyClient.openToReceive 
                    (false, Session.AUTO_ACKNOWLEDGE, 
                        "JMSCorrelationID='" + correlationID + "'");

                QueueSender sender = client.openToSend ();
                Message request = client.getSession ().createTextMessage (XML);
                
                if (correlationID != null)
                {
                    request.setJMSCorrelationID (correlationID);
                }
                if (setReplyTo)
                {
                    request.setJMSReplyTo(replyClient.getQueue ());
                }
                
                sender.send (request);

                Message response = receiver.receive (15000);

                if (correlationID == null || !setReplyTo || !expectResponse)
                {
                    assertNull(response);
                    return;
                }
                
                assertNotNull(response);
                assertTrue (response instanceof TextMessage);
                
                System.out.println (((TextMessage) response).getText ());
                Trade confirmedTrade = TradeMessenger.tradeFromXML 
                    (((TextMessage) response).getText ());
                assertEquals (trade.getId (), confirmedTrade.getId ());
                assertEquals (sharesExpected, confirmedTrade.getSize ());
                assertEquals (resultExpected, confirmedTrade.getResult ());
            }
        }
    }
    
    @Test
    public void testSuccess()
        throws Exception 
    {
        OrderBroker.outcomeGenerator = () -> 100;
        sendTradeRequest ("TestCorrelationID_" + System.currentTimeMillis (), 
            true, true, TradeFactory.BUY_SIZE, Trade.Result.FILLED);
        
    }
    
    @Test
    public void testPartial()
        throws Exception 
    {
        OrderBroker.outcomeGenerator = () -> 15;
        sendTradeRequest ("TestCorrelationID_" + System.currentTimeMillis (), 
            true, true, TradeFactory.BUY_SIZE * 3 / 5, Trade.Result.PARTIALLY_FILLED);
        
    }
    
    @Test
    public void testRejection()
        throws Exception 
    {
        OrderBroker.outcomeGenerator = () -> 4;
        sendTradeRequest ("TestCorrelationID_" + System.currentTimeMillis (), 
            true, true, 0, Trade.Result.REJECTED);
        
    }
    
    @Test
    public void testNoResponse()
        throws Exception 
    {
        OrderBroker.outcomeGenerator = () -> 1;
        sendTradeRequest ("TestCorrelationID_" + System.currentTimeMillis (), 
            true, false, 0, null);
        
    }
    
    @Test
    public void testNoCorrelationID()
        throws Exception 
    {
        sendTradeRequest (null, false, false, 0, null);
        
    }
    
    @Test
    public void testNoResponseQueue()
        throws Exception 
    {
        sendTradeRequest ("TestCorrelationID_" + System.currentTimeMillis (), 
            false, false, 0, null);
        
    }
    
    /**
    Enable this if testing gets queues gooped up, but generally ignore:
    otherwise we can kill replies in the real, running queues.
    */
    @After @Ignore 
    public void cleanReplyQueue ()
        throws Exception
    {
        try ( PointToPointClient replyClient = 
            new PointToPointClient (TradeMessenger.RESPONSE_QUEUE); )
        {
            QueueReceiver receiver = replyClient.openToReceive (); 
            while (receiver.receive (500) != null)
                System.out.println ("Cleaned out message.");
        }
    }
}
