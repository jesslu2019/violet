package com.citi.trading.mq;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import com.citi.trading.Trade;

/**
Utility for basic persistence testing.

@author Will Provost
*/
public class TradeFactory
{
    public static final String BUY_STOCK = "HON";
    public static final int BUY_SIZE = 2000;
    public static final double BUY_PRICE = 88;

    public static final String SALE_STOCK = "GE";
    public static final int SALE_SIZE = 1000;
    public static final double SALE_PRICE = 100;

    /**
    Factory for a simple trade object representing a purchase.
    */
    public static Trade createTestPurchase ()
    {
        Trade trade = new Trade ();
        trade.setWhen (new Timestamp (System.currentTimeMillis ()));
        trade.setStock (BUY_STOCK);
        trade.setBuy (true);
        trade.setSize (BUY_SIZE);
        trade.setPrice (BUY_PRICE);
        return trade;
    }
    
    /**
    Helper to assert that a given object has the same values as
    our {@link #createTestPurchase standard purchase}.
    */
    public static void assertEqualsTestPurchase (Trade candidate)
    {
        assertEquals (BUY_STOCK, candidate.getStock ());
        assertEquals (true, candidate.isBuy ());
        assertEquals (BUY_SIZE, candidate.getSize ());
        assertEquals (BUY_PRICE, candidate.getPrice (), 0.0001);
    }

    /**
    Factory for a simple trade object representing a sale.
    */
    public static Trade createTestSale ()
    {
        Trade trade = new Trade ();
        trade.setWhen (new Timestamp (System.currentTimeMillis ()));
        trade.setStock (SALE_STOCK);
        trade.setBuy (false);
        trade.setSize (SALE_SIZE);
        trade.setPrice (SALE_PRICE);
        return trade;
    }
    
    /**
    Factory for a simple trade object representing a successful sale response.
    */
    public static Trade createTestSaleSuccess ()
    {
        Trade trade = createTestSale ();
        trade.setResult (Trade.Result.FILLED);
        return trade;
    }
    
    /**
    Helper to assert that a given object has the same values as
    our {@link #createTestSale standard sale}.
    */
    public static void assertEqualsTestSale (Trade candidate)
    {
        assertEquals (SALE_STOCK, candidate.getStock ());
        assertEquals (false, candidate.isBuy ());
        assertEquals (SALE_SIZE, candidate.getSize ());
        assertEquals (SALE_PRICE, candidate.getPrice (), 0.0001);
    }
    
    /**
    Helper to assert that a given object has the same values as
    our {@link #createTestSale standard sale}.
    */
    public static void assertEqualsTestSaleSuccess (Trade candidate)
    {
        assertEqualsTestSale (candidate);
        assertEquals (Trade.Result.FILLED, candidate.getResult ());
    }
}
